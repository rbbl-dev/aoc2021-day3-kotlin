import java.io.File

fun main() {
    val lines = File("input.txt").readLines()
    val generatorString = getReading(lines, ReadingType.GENERATOR)
    val scrubberString = getReading(lines, ReadingType.SCRUBBER)
    val generatorValue = Integer.parseInt(generatorString, 2)
    val scrubberValue = Integer.parseInt(scrubberString, 2)
    val lifeSupportRating = generatorValue * scrubberValue
    println("generator: $generatorValue scrubber: $scrubberValue life-support-rating: $lifeSupportRating")
}

fun getReading(list: List<String>, readingType: ReadingType): String {
    var resultList = list
    for (i in list[0].indices) {
        val filterChar = getFilterChar(resultList, i, readingType)
        resultList = resultList.filter { it[i] != filterChar }
        if (resultList.size == 1) {
            return resultList.first()
        }
    }
    throw UnknownError()
}

fun getFilterChar(list: List<String>, scanIndex: Int, readingType: ReadingType): Char {
    var zeroCount = 0
    var oneCount = 0
    list.forEach { line ->
        if (line[scanIndex] == '0') {
            zeroCount++
        }else {
            oneCount++
        }
    }
    return if (readingType == ReadingType.GENERATOR) {
        if (zeroCount == oneCount) {
            '0'
        } else if (zeroCount > oneCount) {
            '1'
        } else {
            '0'
        }
    } else {
        if (zeroCount == oneCount) {
            '1'
        } else if (zeroCount < oneCount) {
            '1'
        } else {
            '0'
        }
    }
}

enum class ReadingType { GENERATOR, SCRUBBER }